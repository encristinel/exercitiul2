package ro.orangetraining;

public class GuessGame {

    public void startGame() {
        System.out.println("I'm thinking of number between 0 and 9...: ");
        boolean game=true;
        while(game){


        Player Player1 = new Player();
        Player Player2 = new Player();
        Player Player3 = new Player();
        int randomNumber=(int)(Math.random()*9);

        System.out.println("Number to guess is:" + randomNumber);

        int P1G=Player1.guess();
        int P2G=Player2.guess();
        int P3G=Player3.guess();

        System.out.println("Player 1: i'm guessing:" + P1G);
        System.out.println("Player 2: i'm guessing:" + P2G);
        System.out.println("Player 3: i'm guessing:" + P3G);
        System.out.println("Player one guessed: " + P1G);
        System.out.println("Player two guessed: " + P2G);
        System.out.println("Player three guessed: " + P3G);

        if(P1G==randomNumber){
        System.out.println("We have a winner!");
        System.out.println("Player one got it right? true");
        System.out.println("Player two got it right? false");
        System.out.println("Player three got it right? false");
        System.out.println("This game is over.");
        game=false;
        }

        else if (P2G==randomNumber) {
            System.out.println("We have a winner!");
            System.out.println("Player one got it right? false");
            System.out.println("Player two got it right? true");
            System.out.println("Player three got it right? false");
            System.out.println("This game is over.");
        game=false;
        }
        else if (P3G==randomNumber) {
            System.out.println("We have a winner!");
            System.out.println("Player one got it right? false");
            System.out.println("Player two got it right? false");
            System.out.println("Player three got it right? true");
            System.out.println("This game is over.");
        game=false;
        }

        else {
            System.out.println("Players will have to try again.");
            game=true;
        }
    }
}}


